import React, {useState, useEffect} from 'react';
import logo from './logo.svg';
import './App.css';


function Todo({todo, index, markTodo, removeTodo, nextTodo}){
  const [buttonText, setButton] = useState("");
  const changeText = (text) => setButton(text);

  return(
    <><div className = "todo">
      {todo.text}
      <button className = 'btn1' onClick = {() => markTodo(index)}>{todo.isDone ? "In Progress" : "Next"}</button>
      <button className = 'btn2' onClick = {() => removeTodo(index)}>Remove</button>
    </div>
  </>
  );
}
function InProgress({todo,index,markTodo,nextTodo,backTodo,markTodoIP})
{
  const [buttonText,setButtonText] = useState("");
  const changeText = (text) => setButtonText(text);

  return (
    <><div className="todo1">
      {todo.text}
      <button className='btn1' onClick={() => backTodo(index)}>{!todo.isDone? "In Progress":"Next"}</button>
        <button className='btn1' onClick={() => markTodoIP(index)}>{todo.nextDone? "Next":"Back"}</button>
        
    </div>
    </>
    
  );

}
function Complete({todo,index,markTodo,removeTodo,backTodo,backTodolast})
{
  const [buttonText,setButtonText] = useState("");
  const changeText = (text) => setButtonText(text);

  return (
    <><div className="todo1">
      {todo.text}
      
        
      <button className='btn1' onClick={() => backTodo(index)}>{!todo.isDone? "":"Back"}</button>
        <button className='btn2' onClick={() => removeTodo(index)}>Remove</button>
    </div>
    </>
    
  );

}

function TodoForm({ addTodo }) {
  const [value, setValue] = React.useState("");

  const handleSubmit = e => {
    e.preventDefault();
    if (!value) return;
    addTodo(value);
    setValue("");
  };

  return (
  <div className='form'>
    <><form onSubmit={handleSubmit}>
      <input
        type="text"
        className="input"
        value={value}
        onChange={e => setValue(e.target.value)}
         />
    
    <button className='btn-3' onClick={handleSubmit}>Submit</button></form></>
    </div>
  );
}

function App() {
  const [todos, setTodos] = React.useState([
    {
      text: "First task",
      isDone: false,
      nextDone:false
    }
  ]);

  const addTodo = text => {
    const newTodos = [...todos, { text }];
    setTodos(newTodos);
  };
  
  const markTodo = index =>{
    const newTodos = [...todos];
    newTodos[index].isDone = !newTodos[index].isDone;
    setTodos(newTodos);
  };


  const completeTodo = index => {
    const newTodos = [...todos];
    newTodos[index].isCompleted = !newTodos[index].isCompleted ;
    setTodos(newTodos);
    console.log(newTodos[index].isCompleted );
    
  };

  const removeTodo = index => {
    const newTodos = [...todos];
    newTodos.splice(index, 1);
    /*
    splice() -> returns an array of elements that have been removed from the array
    */
    setTodos(newTodos);
  };
  const nextTodo = index => {
    const newTodos = [...todos];
    newTodos[index].nextDone = !newTodos[index].nextDone;
    setTodos(newTodos);
   
  }
  const backTodo = index =>{
    const newTodos = [...todos];
    newTodos[index].nextDone = !newTodos[index].nextDone;
    setTodos(newTodos);
  }
  const markTodoIP = index => {
    const newTodos = [...todos];
    newTodos[index].isDone = !newTodos[index].isDone;
    setTodos(newTodos);
  };
 

  return (
    <div className="app">
      <div className="add-list">
        <h1>ToDo</h1>
        <TodoForm addTodo={addTodo} />
        <div className="in-progress">
            <table>
              <thead>ToDo</thead>
                <tr>
                  <td>
                  {todos.map((todo, index) => (
                  <>
                 {(!todo.isDone && <Todo
                    key={index}
                    index={index}
                    todo={todo}
                    markTodo = {markTodo}
                    // markTodo={markTodoIP}
                    removeTodo={removeTodo}
                  />) 
              
                  }
                  </>
                ))}
                  </td>
                </tr>
            </table>
        </div>

        <div className="complete">
          <table>
            <thead>In progress</thead>
            <tr>
              <td>
              {todos.map((todo, index) => (
              <>
                  {(todo.isDone && !todo.nextDone && <InProgress
                    key={index}
                    index={index}
                    todo={todo}
                    nextTodo = {nextTodo}
                    backTodo={backTodo}
                    markTodoIP = {markTodoIP}
                    />) 
                  }
                  </>
             ))}
              </td> 
            </tr>
                
          </table>
        </div>
        
        <div className="complete">
          <table>
            <thead>Completed</thead>
            <tr>
              <td>
              {todos.map((todo, index) => (
              <>
                  {(todo.nextDone && <Complete
                    key={index}
                    index={index}
                    todo={todo}
                    markTodo = {markTodo}
                    removeTodo = {removeTodo}
                    backTodo = {backTodo}
                    />) 
                  }
                  </>
             ))}
              </td> 
            </tr>
                
          </table>
        </div>
      </div>
    </div>
  );
}

export default App;